package prueba;

import historial.Casilla;
import pieza.Caballo;
import historial.Historial;
import tablero.Tablero;

public class pruebas {

    public static void main(String[] args) {
        Tablero tablero = new Tablero(4);
        Historial historial1 = new Historial();
        Casilla casilla1 = new Casilla(1,1);
        Caballo caballo = new Caballo(casilla1, tablero, historial1);
              
        caballo.mover(new Casilla(3,2));
        caballo.mover(new Casilla(2,4));
        caballo.mover(new Casilla(4,3));
        caballo.mover(new Casilla(5,1));
        caballo.mover(new Casilla(7,2));
        caballo.mover(new Casilla(8,4));
        caballo.mover(new Casilla(6,3));
        //caballo.imprimirHistorial();
        tablero.agregarCaballo(caballo);
        System.out.println("primeros movimientos");
        tablero.imprimirTablero(tablero.getCaballos().get(1));
        System.out.println(caballo.getCasilla());
        
        tablero.aumentarDimension();
        System.out.println("despues de haber incrementado el tablero");
        tablero.imprimirTablero(tablero.getCaballos().get(1));
        System.out.println(caballo.getHistorial().getHistorial());
        System.out.println(caballo.getCasilla());
        
        caballo.regresar();
        System.out.println("regresa 2");
        tablero.imprimirTablero(tablero.getCaballos().get(1));  
        System.out.println(caballo.getHistorial().getHistorial());
        System.out.println(caballo.getCasilla());
        
        caballo.avanzar();
        System.out.println(caballo.getHistorial().getHistorial());
        System.out.println("avanza uno");
        tablero.imprimirTablero(tablero.getCaballos().get(1));
        System.out.println(caballo.getHistorial().getHistorial());
        System.out.println(caballo.getCasilla());
        
        caballo.mover(new Casilla(10,7));
        System.out.println("se mueve a una nueva casilla");
        System.out.println(tablero.getCaballos().get(1).getHistorial().getHistorial().get(tablero.getCaballos().get(1).getHistorial().getHistorial().size()));
        tablero.imprimirTablero(tablero.getCaballos().get(1));
        System.out.println(caballo.getHistorial().getHistorial());
        System.out.println(caballo.getCasilla());
        
        caballo.regresar();
        System.out.println("regresa uno");
        System.out.println(tablero.getCaballos().get(1).getHistorial().getHistorial().get(tablero.getCaballos().get(1).getHistorial().getHistorial().size()));
        tablero.imprimirTablero(tablero.getCaballos().get(1));
        System.out.println(caballo.getHistorial().getHistorial());
        System.out.println(caballo.getCasilla());
        
        tablero.disminuirDimension();
        System.out.println("dsiminulle la dimension");
        tablero.imprimirTablero(tablero.getCaballos().get(1));
        System.out.println(caballo.getHistorial().getHistorial());
        System.out.println(caballo.getCasilla());
        
        tablero.aumentarDimension();
        System.out.println("aumenta en uno");
        tablero.imprimirTablero(tablero.getCaballos().get(1));
        System.out.println(caballo.getHistorial().getHistorial());
        System.out.println(caballo.getCasilla());
    }

}
