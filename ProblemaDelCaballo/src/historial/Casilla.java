/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package historial;

import tablero.Tablero;


public class Casilla {
    
    private Integer fila;
    private Integer columna;
    private Character columnaEnLetra;
    
    public Casilla (Integer fila, Integer columna){
        if(fila == null || columna == null)
            throw new IllegalArgumentException();
        if(fila < 1 && fila > 26){
            this.fila = 1;
        }
        if( columna  < 1 &&  columna > 26){
            this.columna = 1;
        }
        else{
            this.fila = fila;
            this.columna = columna;
        }
        this.columnaEnLetra  =  buscarLetradeColumna(columna);
    }
    
    private Character buscarLetradeColumna(Integer columna){
        char alfabeto [] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        return alfabeto[columna-1];               
    }
    
    public Integer getFila(){
        return fila;
    }
    
    public Integer getColumna(){
        return columna;
    }
    
    public Character getLetra(){
        return columnaEnLetra;
    }
    
    public void setCasilla(Casilla casilla){
        this.fila = casilla.getFila();
        this.columna = casilla.getColumna();
        this.columnaEnLetra = casilla.getLetra();
    }
    
    public boolean estaEnRango(Tablero tablero){
        if(fila > 0 && columna > 0 && fila <= tablero.getDimension() && columna <= tablero.getDimension()){
            return true;
        }
       return false;
    }
    
     public String toString(){
         return "{"
                 + " f: "+fila
                 + " c: "+columna
                 + " l: "+columnaEnLetra
                 +"}";
     }
}
