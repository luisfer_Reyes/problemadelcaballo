/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package historial;

import java.util.LinkedHashMap;
import java.util.Map;
import tablero.Tablero;

/**
 *
 * @author luisfer
 */
public class Historial {
    
    private Map<Integer,Casilla> historial;
    
    public Historial (){
        this.historial = new LinkedHashMap();
    }
    
     public void agregarAlHistorial(Casilla casilla){
        historial.put(historial.size()+1, casilla);
    }
    
    public Integer tamanioHistorial(){
        return historial.size();
    }
    
    public void borrarEnHistorial (Integer posicion){
        for(Integer indice =  posicion ; indice <= historial.size(); indice ++ )
            historial.remove(indice);
    }
    
    /* public boolean historialValido(){
         for (Integer indice = 1; indice <= historial.size(); indice ++)
             if(historial.get(indice).get("fila")> historial.size() || historial.get(indice).get("columna")>historial.size())
                 return false;
         return true;
     }*/
     
    public Boolean buscarEnHistorial(Casilla casilla){
        for(Integer indiceHistorial=1; indiceHistorial <= historial.size(); indiceHistorial ++){
               if(historial.get(indiceHistorial).equals(casilla))
                    return true;
        }
        return false;         
    }
    
    public Integer posicionEnHistorial (Casilla casilla){
       for (Integer indice = 1; indice < historial.size(); indice++){
          // System.out.println(casilla +" --- "+(historial.get(indice)));
           if(casilla.equals(historial.get(indice)))
               return indice;   
       }
       return historial.size();
    }
    
    public void eliminarEnHitorialNoValidos(Tablero tablero){
       for (Integer indice = 1; indice <= historial.size(); indice ++)
             if(historial.get(indice).getFila()> tablero.getDimension() || historial.get(indice).getColumna()>tablero.getDimension())
                 historial.remove(indice);
    }
    
    public void actualizarHistorial(Integer cambio){
        for(Integer indice = 1; indice <= historial.size(); indice ++){
            historial.get(indice).setCasilla(new Casilla (historial.get(indice).getFila()+cambio, historial.get(indice).getColumna()+cambio));
        }
       
    }
    
    public Map<Integer,Casilla> getHistorial(){
        return historial;
    }
    
    public String toString(){
        String s = "{";
        for(Integer indice = 1; indice < historial.size(); indice ++){
            s = s+historial.toString() +"    ";
        }
        return s += " }";
    }
}
