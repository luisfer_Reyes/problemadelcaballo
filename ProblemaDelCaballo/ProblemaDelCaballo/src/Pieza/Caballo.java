/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pieza;

import Tablero.Tablero;

/**
 *
 * @author Administrador
 */
public class Caballo implements Pieza{
    private String nombre;
    private int fila;
    private int columna;
    
    public Caballo(int fila,int columna){
        nombre="caballo";
        this.fila=fila;
        this.columna=columna;
    }
    
    public int getFila(){
        return fila;
    }
    
    public int getColumna(){
        return columna;
    }
    
    public void setFila(int fila){
        this.fila=fila;
    }
    
    public void setColumna(int columna){
        this.columna=columna;
    }
    
    public Boolean movimientoPosible(int fila,int columna){
            if(fila == getFila()+1 && columna == getColumna()+2){
                    return true;
                }
            if(fila == getFila()-1 && columna == getColumna()+2){
                    return true;
                }
            if(fila == getFila()-2 && columna == getColumna()+1){
                    return true;
                }
            if(fila == getFila()-2 && columna == getColumna()-1){
                   return true;
                }
            if(fila ==getFila()-1 && columna == getColumna()-2){
                    return true;
                }
            if(fila == getFila()+1 && columna == getColumna()-2){
                   return true;
                }
            if(fila ==getFila()+2 && columna ==getColumna()-1){
                   return true;
                }
            if(fila == getFila()+2 && columna == getColumna()+1){
                   return true;
                }
        return false;
    }
}
